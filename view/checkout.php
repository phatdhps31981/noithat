<!-- Start Hero Section -->
<div class="hero">
				<div class="container">
					<div class="row justify-content-between">
						<div class="col-lg-5">
							<div class="intro-excerpt">
								<h1>Thanh Toán</h1>
							</div>
						</div>
						<div class="col-lg-7">
							
						</div>
					</div>
				</div>
			</div>
		<!-- End Hero Section -->

		<div class="untree_co-section">
		    <div class="container">
				<?php if(!isset($_SESSION['role'])){
					echo 
					'
				<div class="row mb-5">
		        <div class="col-md-12">
		          <div class="border p-4 rounded" role="alert">
				  Phản hồi khách hàng? <a href="index.php?page=login">Bấm vào đây</a> Để đăng nhập.
		          </div>
		        </div>
		      	</div>
					';
					}
				?>
		<form action="index.php?page=paybill" method="post">
			<?php 
				$kqbill = getall_billuser();
				$_SESSION['idbill'] = $kqbill[0]['id'];
				$_SESSION['addressbill'] = $kqbill[0]['address'];
				$_SESSION['emailbill'] = $kqbill[0]['email'];
				$_SESSION['sdtbill'] = $kqbill[0]['sdt'];
				$_SESSION['tenbill'] = $kqbill[0]['name'];
			?>

		      <div class="row">
		        <div class="col-md-6 mb-5 mb-md-0">
		          <h2 class="h3 mb-3 text-black">Chi tiết thanh toán</h2>
		          <div class="p-3 p-lg-5 border bg-white">
		            <div class="form-group row">
		              <div class="col-md-12">
		                <label for="c_fname" class="text-black">Họ Và Tên <span class="text-danger">*</span></label>
						<input type="hidden" name="id">
		                <input type="text" class="form-control" id="c_fname" name="c_fname" value="<?php 
							if(isset($_SESSION['idsuser'])) {
								echo $_SESSION['tenbill'];
							}else {
								$_SESSION['tenbill'] = null;
							}?>">
		              </div>
		            </div>

		            <div class="form-group row">
		              <div class="col-md-12">
		                <label for="c_address" class="text-black">Địa chỉ <span class="text-danger">*</span></label>
		                <input type="text" class="form-control" id="c_address" name="c_address" placeholder="Nhập địa chỉ" value="<?php 
							if(isset($_SESSION['idsuser'])) {
								echo $_SESSION['addressbill'];
							}else {
								$_SESSION['addressbill'] = null;
							}
						?>">
		              </div>
		            </div>


		            <div class="form-group row mb-5">
		              <div class="col-md-6">
		                <label for="c_email_address" class="text-black">Địa chỉ email <span class="text-danger">*</span></label>
		                <input type="text" class="form-control" id="c_email_address" name="c_email_address" value="<?php 
							if(isset($_SESSION['idsuser'])) {
								echo $_SESSION['emailbill'];
							}else {
								$_SESSION['emailbill'] = null;
							}
						?>">
		              </div>
		              <div class="col-md-6">
		                <label for="c_phone" class="text-black">Số Điện Thoại  <span class="text-danger">*</span></label>
		                <input type="text" class="form-control" id="c_phone" name="c_phone" placeholder="Số điện thoại" value="<?php 
							if(isset($_SESSION['idsuser'])) {
								echo $_SESSION['sdtbill'];
							}else {
								$_SESSION['sdtbill'] = null;
							}
						?>">
		              </div>
		            </div>
					<?php 
						if(isset($nameErr)){
							echo '<div class="form-group row">
									<p class ="error_text">'.$nameErr.'</p>
								</div>';
						}else {
							unset($nameErr);
						}
					?>

		            <div class="form-group">
		              <label for="c_order_notes" class="text-black">Ghi chú đặt hàng</label>
		              <textarea name="c_order_notes" id="c_order_notes" cols="30" rows="5" class="form-control" placeholder="Viết ghi chú của bạn ở đây ..."></textarea>
		            </div>

		          </div>
		        </div>
		        <div class="col-md-6">

		          <div class="row mb-5">
		            <div class="col-md-12">
		              <h2 class="h3 mb-3 text-black"> Mã giảm giá</h2>
		              <div class="p-3 p-lg-5 border bg-white">
						<form action="index.php?page=checksale" method="post">
		                <label for="c_code" class="text-black mb-3">Nhập mã phiếu giảm giá của bạn nếu bạn có một</label>
		                <div class="input-group w-75 couponcode-wrap">
		                  <input type="text" class="form-control me-2" id="c_code" name="c_code" placeholder="Mã giảm giá" aria-label="Coupon Code" aria-describedby="button-addon2">
		                  <div class="input-group-append">
		                    <button class="btn btn-black btn-sm" type="submit" id="button-addon2" name="apply" value="Apply">Áp dụng</button>
		                  </div>
		                </div>
						<?php
						  	if(isset($error)){
								echo '
								<font color="red">'.$error.'</font>';
							}else {
								unset($error);
							}
						?>
						</form>
		              </div>
		            </div>
		          </div>

		          <div class="row mb-5">
		            <div class="col-md-12">
		              <h2 class="h3 mb-3 text-black">Đơn hàng của bạn</h2>
		              <div class="p-3 p-lg-5 border bg-white">
		                <table class="table site-block-order-table mb-5">
		                  <thead>
		                    <th>Sản phẩm</th>
		                    <th>Tổng</th>
		                  </thead>
		                  <tbody>
							<?php
							$totalAll = 0;
							if (isset($_SESSION['viewcart']) && is_array($_SESSION['viewcart'])) {
								  foreach ($_SESSION['viewcart'] as $item) {                                                    
									extract($item);
									$totalAll += $total;  
									echo '
										<tr>
										<td>'.$ten.'<strong class="mx-2">x</strong>'.$soluong.'</td>
										<td>'.number_format($total, 0, ',', '.').' VND</td>
										</tr>
									  ';
								  }
								}
							?>
		                   
								<?php
								if(isset($totalAllsale)){
									$kq = Coupon($code);
									foreach ($getcode as $getcd){
										echo '
										<tr>
											<td>Mã giảm giá:</td> 
											<td>'.$getcd['makm'].'    Giảm: '.$getcd['phantramkm'].'%</td>
										<tr>';
									}
									echo '
									<tr>
										<td class="text-black font-weight-bold"><strong>Tổng số đặt hàng</strong></td>
										<td class="text-black font-weight-bold"><strong>'. number_format($totalAllsale, 0, ',', '.') .' VND</strong></td>
									</tr>';
								}else {
									echo '
									<tr>
										<td class="text-black font-weight-bold"><strong>Tổng số đặt hàng</strong></td>
										<td class="text-black font-weight-bold"><strong>'. number_format($totalAll, 0, ',', '.') .' VND</strong></td>
									</tr>';
								}
								?>
		               
		                  </tbody>
		                </table>

		                <div class="border p-3 mb-3">
		                  <h3 class="h6 mb-0"><a class="d-block" data-bs-toggle="collapse" href="#collapsebank" role="button" aria-expanded="false" aria-controls="collapsebank">Thanh Toán Khi Nhận Hàng</a></h3>

		                  <div class="collapse" id="collapsebank">
		                    <div class="py-2">
		                      <p class="mb-0">Thực hiện thanh toán trực tiếp vào tài khoản ngân hàng của chúng tôi. Vui lòng sử dụng ID đơn đặt hàng của bạn làm tài liệu tham khảo thanh toán. Đơn đặt hàng của bạn đã giành được được vận chuyển cho đến khi các khoản tiền đã được xóa trong tài khoản của chúng tôi.</p>
		                    </div>
		                  </div>
		                </div>

		                <div class="border p-3 mb-3">
		                  <h3 class="h6 mb-0"><a class="d-block" data-bs-toggle="collapse" href="#collapsecheque" role="button" aria-expanded="false" aria-controls="collapsecheque">Thanh Toán Trực tuyến</a></h3>

		                  <div class="collapse" id="collapsecheque">
		                    <div class="py-2">
		                      <p class="mb-0">Thực hiện thanh toán trực tiếp vào tài khoản ngân hàng của chúng tôi. Vui lòng sử dụng ID đơn đặt hàng của bạn làm tài liệu tham khảo thanh toán. Đơn đặt hàng của bạn đã giành được được vận chuyển cho đến khi các khoản tiền đã được xóa trong tài khoản của chúng tôi.</p>
		                    </div>
		                  </div>
		                </div>

		                <div class="border p-3 mb-5">
		                  <h3 class="h6 mb-0"><a class="d-block" data-bs-toggle="collapse" href="#collapsepaypal" role="button" aria-expanded="false" aria-controls="collapsepaypal">Paypal</a></h3>

		                  <div class="collapse" id="collapsepaypal">
		                    <div class="py-2">
		                      <p class="mb-0">Quét mã QR để Thanh Toán</p>
		                    </div>
		                  </div>
		                </div>

		                <div class="form-group">
						<a href="index.php?page=thankyou">
		                  <input type="submit" class="btn btn-black btn-lg py-3 btn-block" name="pay" value="Thanh Toán">
						</a>
		                </div>
		              </div>
		            </div>
		          </div>
		        </div>
		      </div>
		    </div>
		  </div>